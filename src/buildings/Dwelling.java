/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.Serializable;
/**
 *
 * @author Павел
 */
public class Dwelling implements Building,Serializable{
    Floor[] Dwell;
    
    public Dwelling(int floors, int[] flats){
        if(floors<=0)
            throw new FloorIndexOutOfBoundsException();
        else{
            for(int i=0;i<flats.length;i++){
                if(flats[i]<=0)
                    throw new SpaceIndexOutOfBoundsException();
            }
        Dwell=new DwellingFloor[floors];
        int i;
        for(i=0;i<flats.length;i++){
            Dwell[i] = new DwellingFloor(flats[i]);
        }
        }
    }
    public Dwelling(Floor[] floors){
        Dwell=floors;
    }
    public int getBuildingFloor(){
        return Dwell.length;
    }
    public int getBuildingSpace(){
        int S=0;
        int i;
        for (i=0;i<Dwell.length;i++){
            S+=Dwell[i].getFloorSpace();
        }
        return S;
    }
    public double getBuildingSquare(){
        int S=0;
        int i;
        for (i=0;i<Dwell.length;i++){
            S+=Dwell[i].getFloorSquare();
        }
        return S;
    }
    public int getBuildingRoom(){
        int S=0;
        int i;
        for (i=0;i<Dwell.length;i++){
            S+=Dwell[i].getFloorRoom();
        }
        return S;
    }
    public Floor[] getBuildingFloorArrray(){
        return Dwell;
    }
    public Floor getFloor(int n){
        if(n<0 || n>getBuildingFloor())
            throw new FloorIndexOutOfBoundsException();
        else
        return Dwell[n];
    }
    public Space getSpaceByIndex(int n){
        if(n<0 || n>getBuildingFloor())
            throw new FloorIndexOutOfBoundsException();
        else{
        int i;
        int j;
        int numb=0;
        for (i=0;i<Dwell.length;i++){
            for(j=0;j<Dwell[i].getFloorSpace();j++){
                numb++;
                if (numb==n)
                    break;
            }
            if (numb==n)
                break;
        }
        return Dwell[i].getSpaceByIndex(numb);
        }
    }
    public void setSpaceByIndex(int n, Space flat){
        if(n<0 || n>getBuildingSpace())
            throw new FloorIndexOutOfBoundsException();
        else{
        int i;
        int j;
        int numb=0;
        for (i=0;i<Dwell.length;i++){
            numb=0;
            for(j=0;j<Dwell[i].getFloorSpace();j++){
                numb++;
                if (numb==n)
                    break;
            }
             if (numb==n)
                    break;
        }
       Dwell[i].setSpaceByIndex(numb, flat);
        }
    }
    public void deleteSpaceByIndex(int n){
        if(n<0 || n>getBuildingFloor())
            throw new FloorIndexOutOfBoundsException();
        else{
        int i;
        int j;
        int numb=0;
        for (i=0;i<Dwell.length;i++){
            for(j=0;j<Dwell[i].getFloorSpace();j++){
                numb++;
                if (numb==n)
                    break;
            }
            if (numb==n)
                    break;
        }
        Dwell[i].deleteSpaceByIndex(n);
        }
    }
    public Space getBestSpace(){
        double max = Dwell[0].getBestSpace().getSquare();
        int i;
        Space mxflat = new Flat(Dwell[0].getBestSpace().getSquare(),Dwell[0].getBestSpace().getRoom());
        for (i=0;i<Dwell.length;i++){
                if (max<Dwell[i].getBestSpace().getSquare()){
                    max = Dwell[i].getBestSpace().getSquare();
                    mxflat.setRoom(Dwell[i].getBestSpace().getRoom());
                    mxflat.setSquare(Dwell[i].getBestSpace().getSquare());
                }
        }
        return mxflat;
    }
    public Space[] getSortSpace(){
        int S=0;
        int D=0;
        for(int i=0;i<Dwell.length;i++){
            for(int j=0;j<Dwell[i].getFloorSpace();j++){
                S++;
            }
        }
        Space[] flats= new Flat[S];
        for(int i=0;i<Dwell.length;i++){
            for(int j=0;j<Dwell[i].getFloorSpace();j++){
                flats[D]= new Flat(Dwell[i].getSpaceByIndex(j).getSquare(),Dwell[i].getSpaceByIndex(j).getRoom());
                D++;
            }
        }
    for(int i = flats.length -1 ; i > 0 ; i--){
        for(int j = 0 ; j < i ; j++){
            if( flats[j].getSquare() < flats[j+1].getSquare()){
                Space tmp = flats[j];
                flats[j] = flats[j+1];
                flats[j+1] = tmp;
            }
        }
    }
return flats;
}

    @Override
    public Floor getFloorByIndex(int index) {
        if(index<0 || index>Dwell.length)
            throw new FloorIndexOutOfBoundsException();
        return Dwell[index];
    }

    @Override
    public void setFloorByIndex(int index, Floor floor) {
        if(index<0 || index>Dwell.length)
            throw new FloorIndexOutOfBoundsException();
        else{
            Dwell[index]=floor;
        }
    }

    @Override
    public void addSpaceByIndex(int index, Space space) {
        if(index<0 || index>getBuildingSpace())
            throw new SpaceIndexOutOfBoundsException();
        else{
            int S = 0;
            for (int i = 0; i < getBuildingFloor(); i++) {
                S += getFloorByIndex(i).getFloorSpace();
                if (S >= index) {
                    getFloorByIndex(i).addSpaceByIndex(index, space);
                    break;
                }
        }
    }
    }
}
