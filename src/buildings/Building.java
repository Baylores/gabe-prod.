/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;

/**
 *
 * @author Павел
 */
public interface Building {
    public abstract int getBuildingFloor();
    public abstract int getBuildingSpace();
    public abstract double getBuildingSquare();
    public abstract int getBuildingRoom();
    public abstract Floor[] getBuildingFloorArrray();
    public abstract Floor getFloorByIndex(int index);
    public abstract void setFloorByIndex(int index, Floor floor);
    public abstract Space getSpaceByIndex(int index);
    public abstract void setSpaceByIndex(int index, Space space);
    public abstract void addSpaceByIndex(int index, Space space);
    public abstract void deleteSpaceByIndex(int index);
    public abstract Space getBestSpace();
    public abstract Space[] getSortSpace();
}
