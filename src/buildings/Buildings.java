/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.*;
/**
 *
 * @author Павел
 */
public class Buildings {
    public static void outputBuilding (Building building,OutputStream out) throws IOException{
        try {
        DataOutputStream out1 = new DataOutputStream (out);
        out1.writeInt(building.getBuildingFloor());
        for(int i=0;i<building.getBuildingFloor();i++){
            out1.writeInt(building.getFloorByIndex(i).getFloorSpace());
            for(int j=0;j<building.getFloorByIndex(i).getFloorSpace();j++){
                out1.writeInt(building.getFloorByIndex(i).getSpaceByIndex(j).getRoom());
                out1.writeDouble(building.getFloorByIndex(i).getSpaceByIndex(j).getSquare());
            }
        }
        out1.close();
        }
    catch(IOException e) {
    System.out.println("Some error occurred!");
    }
    }
    public static Building inputBuilding (InputStream in) throws IOException{
        DataInputStream in1= new DataInputStream(in);
        Floor[] floor=new Floor[in1.readInt()];
        Building build=new Dwelling(floor);
        try{
        for(int i=0;i<floor.length;i++){
            int S=in1.readInt();
            for(int j=0;j<S;j++){
                floor[i].setSpaceByIndex(j, new Flat(in1.readDouble(),in1.readInt()));
            }
        }
        in1.close();
        }
    catch(IOException e) {
    System.out.println("Some error occurred!");
    }
    return build;
    }
    public static void writeBuilding (Building building, Writer out) throws IOException{
        PrintWriter out1= new PrintWriter(out);
        out1.print(building.getBuildingFloor());
        for(int i=0;i<building.getBuildingFloor();i++){
            out1.print(" " + building.getFloorByIndex(i).getFloorSpace());
            for(int j=0;j<building.getFloorByIndex(i).getFloorSpace();j++){
                out1.print(" " + building.getFloorByIndex(i).getSpaceByIndex(j).getRoom());
                out1.print(" " + building.getFloorByIndex(i).getSpaceByIndex(j).getSquare());
            }
        }
        out1.close();
        }
    public static Building readBuilding (Reader in) throws IOException{
        StreamTokenizer st = new StreamTokenizer(in);
        st.nextToken();
        DwellingFloor[] floor=new DwellingFloor[(int)st.nval];
        Building build=new Dwelling(floor);
        int R=0;
        double s=0;
        for(int i=0;i<floor.length;i++){
            st.nextToken();
            int S=(int)st.nval;
            floor[i]=new DwellingFloor(S);
            for(int j=0;j<S;j++){
                st.nextToken();
                R=(int)st.nval;
                st.nextToken();
                s=st.nval;
                floor[i].setSpaceByIndex(j, new Flat(s,R));
            }
        }
    return build;
    }
}
