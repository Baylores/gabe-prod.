/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;

/**
 *
 * @author Павел
 */
public class PlacementExchanger {
    public static boolean checkExchangeSpace(Space space1, Space space2){
        boolean ans=false;
        if((space1.getRoom()==space2.getRoom()) && (space1.getSquare()==space2.getSquare()))
            ans=true;
        return ans;
    }
    public static boolean checkExchangeFloor(Floor floor1,Floor floor2){
        boolean ans=false;
        if((floor1.getFloorRoom()==floor2.getFloorRoom()) && (floor1.getFloorSquare()==floor2.getFloorSquare()))
            ans=true;
        return ans;
    }
    public static void exchangeFloorRooms(Floor floor1, int index1, Floor floor2,int index2) throws InexchangeableSpacesException{
        if(index1<0 || index1>floor1.getFloorSpace() || index2<0 || index2>floor2.getFloorSpace())
            throw new InexchangeableSpacesException();
        else{
            if(checkExchangeSpace(floor1.getSpaceByIndex(index1), floor2.getSpaceByIndex(index2))){
                Space tmp=floor1.getSpaceByIndex(index1);
                floor1.setSpaceByIndex(index1, floor2.getSpaceByIndex(index2));
                floor2.setSpaceByIndex(index2, tmp);
            }
            else
                throw new InexchangeableSpacesException();
        }
    }
    public static void exchangeBuildingFloors(Building building1, int index1, Building building2, int index2)throws InexchangeableFloorsException{
        if(index1<0 || index1>building1.getBuildingFloor() || index2<0 || index2>building2.getBuildingFloor())
            throw new InexchangeableFloorsException();
        else{
            if(checkExchangeFloor(building1.getFloorByIndex(index1),building2.getFloorByIndex(index2))){
                Floor tmp=building1.getFloorByIndex(index1);
                building1.setFloorByIndex(index1, building2.getFloorByIndex(index2));
                building2.setFloorByIndex(index2, tmp);
            }
            else
                throw new InexchangeableFloorsException();
        }
    }
}
