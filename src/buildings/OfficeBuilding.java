/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.Serializable;
/**
 *
 * @author Павел
 */
public class OfficeBuilding implements Building,Serializable{

    private Node2 head;
    private int buildingflr;
    private int buildingoff;
    private double buildingsquare;
    private int buildingroom;

    class Node2 {

        Floor officefloor;
        Node2 next;
        Node2 prev;

        Node2(int n) {
            officefloor = new OfficeFloor(n);
        }

        Node2(Floor officeflr) {
            officefloor = officeflr;
        }
    }

    private Node2 getNodeByIndex(int index) {
        if (index < 0 || index > getBuildingFloor()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            Node2 elem = this.head;
            while (index != 0) {
                elem = elem.next;
                index--;
            }
            return elem;
        }
    }

    private void addNodeToTail(Floor officeflr) {
        if (head == null) {
            head = new Node2(officeflr);
        } else {
            if (head.next == null) {
                head.next = new Node2(officeflr);
                head.next.next = head;
                head.prev = head.next;
                head.next.prev = head;
            } else {
                Node2 a = head;
                do {
                    a = a.next;
                } while (a.next != head);
                a.next = new Node2(officeflr);
                a.next.prev = a;
                a.next.next = head;
                head.prev = a.next;
            }
        }
    }

    private void addNodeByIndex(int index, Floor officeflr1) {
        if (index < 0 || index >= getBuildingFloor()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            if (index == getBuildingFloor()) {
                Node2 a = head;
                while (a.next != head) {
                    a = a.next;
                }
                a.next = new Node2(officeflr1);
                a.next.next = head;
                a.next.prev = a;
                head.prev = a.next.next;
            }
            Node2 fn = new Node2(officeflr1);
            Node2 tmp = getNodeByIndex(index);
            fn.prev = tmp.prev;
            tmp.prev.next = fn;
            fn.next = tmp;
            tmp.prev = fn;
            if (index == 0) {
                this.head = fn;
            }
        }
    }

    private void deleteNodeByIndex(int index) {
        if (index <= 0 || index >= getBuildingFloor()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            Node2 tmp = getNodeByIndex(index - 1);
            tmp.next = tmp.next.next;
            tmp.next.next.prev = tmp;
        }
    }

    public OfficeBuilding(int n, int[] off) {
        if (n <= 0) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            this.buildingflr = n;
            int S=0;
            for(int i =0;i<off.length;i++)
                S+=off[i];
            this.buildingoff=S;
            double C=0;
            int R=0;
            Node2 first = new Node2(off[0]);
            this.head = first;
            C+=250;
            R+=1;
            for (int i = 1; i < n; i++) {
                C+=250;
                R+=1;
                first.next = new Node2(off[i]);
                first.next.prev = first;
                first = first.next;
//            addNodeByIndex(i, new OfficeFloor(off[i]));
            }
            this.buildingsquare=C;
            this.buildingroom=R;
            first.next = this.head;
            this.head.prev = first;
        }
    }

    public OfficeBuilding(Floor[] officeflr) {
        this.buildingflr = officeflr.length;
        int S=0;
        for(int i=0;i<officeflr.length;i++){
            S+=officeflr[i].getFloorSpace();
        }
            this.buildingoff=S;
            double C=0;
            int R=0;
        Node2 first = new Node2(officeflr[0]);
        this.head = first;
        C+=officeflr[0].getFloorSquare();
        R+=officeflr[0].getFloorRoom();
        for (int i = 1; i < officeflr.length; i++) {
            C+=officeflr[i].getFloorSquare();
            R+=officeflr[i].getFloorRoom();
            first.next = new Node2(officeflr[i]);
            first.next.prev = first;
            first = first.next;
//        addNodeByIndex(i, officeflr[i]);
        }
        this.buildingroom=R;
        this.buildingsquare=C;
        first.next = this.head;
        this.head.prev = first;
    }

    public int getBuildingFloor() {
        return buildingflr;
    }

    public int getBuildingSpace() {
        return buildingoff;
    }

    public double getBuildingSquare() {
        return this.buildingsquare;
    }

    public int getBuildingRoom() {
        return this.buildingroom;
    }

    public Floor[] getBuildingFloorArrray() {
        Floor[] officeflr = new OfficeFloor[getBuildingFloor()];
        for (int i = 0; i < officeflr.length; i++) {
            officeflr[i] = getNodeByIndex(i).officefloor;
        }
        return officeflr;
    }

    public Floor getFloorByIndex(int index) {
        if (index < 0 || index > getBuildingFloor()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            return getNodeByIndex(index).officefloor;
        }
    }

    public void setFloorByIndex(int index, Floor officeflr) {
        if (index < 0 || index > getBuildingFloor()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            Node2 a = getNodeByIndex(index);
            a.officefloor = officeflr;
        }
    }

    public Space getSpaceByIndex(int index) {
        if (index < 0 || index > getBuildingSpace()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            int S = 0;
            Space office = new Office();
            for (int i = 0; i < getBuildingFloor(); i++) {
                S += getBuildingFloorArrray()[i].getFloorSpace();
                if (S >= index) {
                    office = getBuildingFloorArrray()[i].getSpaceByIndex(S - index);
                }
            }
            return office;
        }
    }

    public void setSpaceByIndex(int index, Space office) {
        if (index < 0 || index > getBuildingSpace()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            int S = 0;
            for (int i = 0; i < getBuildingFloor(); i++) {
                S += getBuildingFloorArrray()[i].getFloorSpace();
                if (S >= index) {
                    getFloorByIndex(i).setSpaceByIndex(S - index, office);
                }
            }
        }
    }

    public void addSpaceByIndex(int index, Space office) {
        if (index < 0 || index > getBuildingSpace()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            int S = 0;
            for (int i = 0; i < getBuildingFloor(); i++) {
                S += getFloorByIndex(i).getFloorSpace();
                if (S >= index) {
                    this.buildingoff+=1;
                    this.buildingroom+=office.getRoom();
                    this.buildingsquare+=office.getSquare();
                    getFloorByIndex(i).addSpaceByIndex(S - getFloorByIndex(i).getFloorSpace()+index, office);
                    break;
                }
            }
        }
    }

    public void deleteSpaceByIndex(int index) {
        if (index < 0 || index >= getBuildingSpace()) {
            throw new FloorIndexOutOfBoundsException();
        } else {
            int S = 0;
            for (int i = 0; i < getBuildingFloor(); i++) {
                S += getBuildingFloorArrray()[i].getFloorSpace();
                if (S >= index) {
                    getFloorByIndex(i).deleteSpaceByIndex(S - index);
                }
            }
        }
    }

    public Space getBestSpace() {
        Space office = new Office();
        Node2 a = head;
        while (a.next != head) {
            office = a.officefloor.getBestSpace();
            a = a.next;
        }
        return office;
    }

    public Space[] getSortSpace() {
        Space[] office = new Space[getBuildingSpace()];
        for (int i = 0; i < office.length; i++) {
            office[i] = getSpaceByIndex(i);
        }
        for (int i = office.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (office[j].getSquare() < office[j + 1].getSquare()) {
                    Space tmp = office[j];
                    office[j] = office[j + 1];
                    office[j + 1] = tmp;
                }
            }
        }
        return office;
    }
}
