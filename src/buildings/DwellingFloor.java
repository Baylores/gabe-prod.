/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.Serializable;
/**
 *
 * @author Павел
 */
public class DwellingFloor implements Floor,Serializable{
    private Space[] dwFlr;
    public DwellingFloor(int n){
        if(n<=0)
            throw new SpaceIndexOutOfBoundsException();
        else{
       this.dwFlr=new Flat[n];
       int i = 0;
       for(i=0;i<n;i++){
           dwFlr[i] = new Flat();
       }
        }
    }
    public DwellingFloor(Space[] DwFlrr){
       this.dwFlr=new Flat[DwFlrr.length];
       int i;
       for (i=0;i<DwFlrr.length;i++){
           dwFlr[i] = new Flat(DwFlrr[i].getSquare(),DwFlrr[i].getRoom());
       }
    }
    public int getFloorSpace(){
        return dwFlr.length;
    }
    public double getFloorSquare(){
        int i;
        double S=0;
        for (i=0;i<dwFlr.length;i++){
            S+=dwFlr[i].getSquare();
        }
        return S;
    }
    public int getFloorRoom(){
        int i;
        int S=0;
        for (i=0;i<dwFlr.length;i++){
            S+=dwFlr[i].getRoom();
        }
        return S;
    }
    public Space[] getFloorSpaceArray(){
        return dwFlr;
    }
    public Space getSpaceByIndex(int n){
        if(n<0 || n>getFloorSpace()){
            throw new SpaceIndexOutOfBoundsException();
        }
        else{
        int i;
        for(i=0;i<dwFlr.length;i++){
            if (i==n) 
                break;
        }
        return dwFlr[i];
        }
    }
    public void setSpaceByIndex(int n,Space flat){
        if(n<0 || n>=getFloorSpace()){
            throw new SpaceIndexOutOfBoundsException();
        }
        else{
        int i;
        for(i=0;i<dwFlr.length;i++){
            if (i==n){
                dwFlr[i].setRoom(flat.getRoom());
                dwFlr[i].setSquare(flat.getSquare());
            }                
        }
        }
    }
    public void addSpaceByIndex(int n, Space flat){
        if(n<0 || n>getFloorSpace()){
            throw new SpaceIndexOutOfBoundsException();
        }
        else{
        Space[] dwFloor = new Space[dwFlr.length+1];
        System.arraycopy(dwFlr, 0, dwFloor, 0, n);
        dwFloor[n+1].setRoom(flat.getRoom());
        dwFloor[n+1].setSquare(flat.getSquare());
        if(dwFlr.length>=n+2)
        System.arraycopy(dwFlr, n+2, dwFloor, n+2, dwFlr.length-n-2);
        }
        }
    public void deleteSpaceByIndex(int n){
        if(n<0 || n>=getFloorSpace()){
            throw new SpaceIndexOutOfBoundsException();
        }
        else{
            Space[] dwFloor = new Space[dwFlr.length];
            System.arraycopy(dwFlr, 0, dwFloor, 0, n);
            System.arraycopy(dwFlr, n + 1, dwFloor, n, dwFlr.length - n -1);
        }
    }
    public Space getBestSpace(){
        Space mxflat = new Flat(dwFlr[0].getSquare(),dwFlr[0].getRoom());
        double max = dwFlr[0].getSquare();
        int i;
        for (i=1;i<dwFlr.length;i++){
            if (max<dwFlr[i].getSquare()){
                max = dwFlr[i].getSquare();
                mxflat.setRoom(dwFlr[i].getRoom());
                mxflat.setSquare(dwFlr[i].getSquare());
            }
        }
        return mxflat;
    }
}
