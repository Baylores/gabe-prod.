/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;

/**
 *
 * @author Павел
 */
public interface Space  {
    public abstract int getRoom();
    public abstract void setRoom(int paramInt);
    public abstract double getSquare();
    public abstract void setSquare(double paramDouble);
}
