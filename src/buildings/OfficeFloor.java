/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.Serializable;
/**
 *
 * @author Павел
 */
public class OfficeFloor implements Floor,Serializable {
    private Node head;
    private int flooroff;
    private int floorroom;
    private double floorsquare;
class Node {
 
Space office;
Node next;
 
Node(double square){
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else{
    this.office = new Office(square);
        }
}
Node(Space office1){
    if(office1.getRoom()<=0)
        throw new InvalidRoomsCountException();
    else{
    if(office1.getSquare()<=0)
        throw new InvalidSpaceAreaException();
        else{
    this.office = new Office(office1.getSquare(),office1.getRoom());
        }
    }
}
Node(){
    this.office = new Office();
}
}
   private Node getNodeByIndex(int index){
       if(index<0 || index>getFloorSpace())
           throw new SpaceIndexOutOfBoundsException();
        else{
    Node elem = this.head;
    while (index != 0)
    {
      elem = elem.next;
      index--;
    }
    return elem;
        } 
   }
private void addNodeToTail(Space office1){
    if(office1.getRoom()<=0)
        throw new InvalidRoomsCountException();
    else{
    if(office1.getSquare()<=0)
        throw new InvalidSpaceAreaException();
        else{
    if(head==null){
        head=new Node(office1);
    }
    else{
    if(head.next==null){
        head.next=new Node(office1);
        head.next.next=head;
    }
    else{
    Node a=head;
    do{
        a=a.next;
    }
    while(a.next!=head);
    a.next=new Node(office1);
    a.next.next = head;
        }
    }
}
}
}
private void addNodeByIndex(int index, Space office1){
    if(office1.getRoom()<=0)
        throw new InvalidRoomsCountException();
    else{
    if(office1.getSquare()<=0)
        throw new InvalidSpaceAreaException();
        else{
        if(index<0 || index>getFloorSpace())
             throw new SpaceIndexOutOfBoundsException();
        else{
    if(index+1 == getFloorSpace()){
        addNodeToTail(office1);
    }
    else{
    Node office = new Node(office1);
    Node tmp1 = getNodeByIndex(index-1);
    Node tmp2 = getNodeByIndex(index);
    tmp1.next = office;
    office.next = tmp2;
    if (index == 0) {
      this.head = office;
    }
    }
        }
        }
    }
}
private void deleteNodeByIndex(int index){
    if(index<0 || index>getFloorSpace())
             throw new SpaceIndexOutOfBoundsException();
        else{
    Node tmp = getNodeByIndex(index-1);
    tmp.next = tmp.next.next;
  }
}

public OfficeFloor(int n){
    if (n<=0)
        throw new SpaceIndexOutOfBoundsException();
    else{
        this.flooroff=n;
        int R=0;
        double S=0;
for(int i=0;i<n;i++){
    R+=1;
    S+=250;
    addNodeToTail(new Office());
}
    this.floorroom=R;
    this.floorsquare=S;
    }
}
public OfficeFloor(Space[] office){
    this.flooroff=office.length;
    int R=0;
    double S=0;
    for(int i=0;i<office.length;i++){
        R+=office[i].getRoom();
        S+=office[i].getSquare();
        addNodeToTail(office[i]);
    }
    this.floorroom=R;
    this.floorsquare=S;
}

public int getFloorSpace(){
    return this.flooroff;
}
public double getFloorSquare(){
    return this.floorsquare;
}
public int getFloorRoom(){       
    return this.floorroom;
}
public Space[] getFloorSpaceArray(){
    Space[] a = new Space[getFloorSpace()];
Node b;
b=head;

for (int i=0;i<getFloorSpace();i++){
    a[i]=b.office;
}
return a;
}
public Space getSpaceByIndex(int index){
    if((index<0) || (index>getFloorSpace()))
             throw new SpaceIndexOutOfBoundsException();
        else
    return getNodeByIndex(index).office;
}
public void setSpaceByIndex(int index, Space office){
    if(office.getRoom()<=0)
        throw new InvalidRoomsCountException();
    else{
    if(office.getSquare()<=0)
        throw new InvalidSpaceAreaException();
        else{
        if(index<0 || index>getFloorSpace())
             throw new SpaceIndexOutOfBoundsException();
        else{
    getNodeByIndex(index).office=office;
}
    }
    }
}
public void addSpaceByIndex(int index, Space office){
    if(office.getRoom()<=0)
        throw new InvalidRoomsCountException();
    else{
    if(office.getSquare()<=0)
        throw new InvalidSpaceAreaException();
        else{
        if(index<0 || (index>getFloorSpace()))
            throw new SpaceIndexOutOfBoundsException();
        else{
            addNodeByIndex(index,office);
        }
    }
    }
}
public void deleteSpaceByIndex(int index){
    if(index<0 || index>getFloorSpace())
             throw new SpaceIndexOutOfBoundsException();
        else
    deleteNodeByIndex(index);
}
public Space getBestSpace(){
    Node max=this.head;
    Node b=head;
    for(int i=0;i<getFloorSpace();i++){
        if(max.office.getSquare()<b.office.getSquare()){
            max=b;
        }
        b=b.next;
    }
    return max.office;
}
}
