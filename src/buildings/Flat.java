/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.Serializable;
/**
 *
 * @author Павел
 */
public class Flat implements Space,Serializable {
    private int room;
    private double square;
    static int ROOM = 2;
    static int SQUARE = 50;
    public Flat(){
        room=ROOM;
        square=SQUARE;
    }
    public Flat(double square){
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else
        {
        this.square=square;
        room=ROOM;
        }
    }
    public Flat(double square, int room){
        if(room<=0){
            throw new InvalidRoomsCountException();
        }
        else{
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else{
        this.room=room;
        this.square=square;
        }
        }
    }
    public int getRoom(){
        return room;
    }
    public double getSquare(){
        return square;
    }
    public void setSquare(double square){
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else
        {
        this.square=square;
        }
    }
    public void setRoom(int room){
        if(room<=0){
            throw new InvalidRoomsCountException();
        }
        else
        this.room=room;
    }
}
