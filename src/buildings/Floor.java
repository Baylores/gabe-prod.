/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;

/**
 *
 * @author Павел
 */
public interface Floor {
    public abstract int getFloorSpace();
    public abstract double getFloorSquare();
    public abstract int getFloorRoom();
    public abstract Space[] getFloorSpaceArray();
    public abstract Space getSpaceByIndex(int index);
    public abstract void setSpaceByIndex(int index,Space space);
    public abstract void addSpaceByIndex(int index,Space space);
    public abstract void deleteSpaceByIndex(int index);
    public abstract Space getBestSpace();
}
