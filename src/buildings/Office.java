/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buildings;
import java.io.Serializable;
/**
 *
 * @author Павел
 */
public class Office implements Space,Serializable {
    private int room;
    private double square;
    static int ROOM = 1;
    static int SQUARE = 250;
    public Office(){
        room=ROOM;
        square=SQUARE;
    }
    public Office(double square){
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else
        {
        this.square=square;
        room=ROOM;
        }
    }
    public Office(double square, int room){
        if(room<=0){
            throw new InvalidRoomsCountException();
        }
        else{
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else{
        this.room=room;
        this.square=square;
        }
        }
    }
    public int getRoom(){
        return room;
    }
    public double getSquare(){
        return square;
    }
    public void setSquare(double square){
        if(square<=0)
            throw new InvalidSpaceAreaException();
        else
        this.square=square;
    }
    public void setRoom(int room){
        if(room<=0){
            throw new InvalidRoomsCountException();
        }
        else
        this.room=room;
    }
}

